import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Structures = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "name",
        header: "name",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "parentId",
        header: "parentId",
        size: 200,
        enableEditing: false,
      },
      {
        accessorKey: "childrenIds",
        header: "childrenIds",
        enableEditing: false,
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "ancestorsIds",
        header: "ancestorsIds",
        enableEditing: false,
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "contactEmail",
        header: "contactEmail",
        enableClickToCopy: true,
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"structures"} />
    </>
  );
};

export default Structures;
