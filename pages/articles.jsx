import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Articles = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "userId",
        header: "userId",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "structure",
        header: "structure",
        enableClickToCopy: true,
        size: 200,
      },
      {
        accessorKey: "title",
        header: "title",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "content",
        header: "content",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "slug",
        header: "slug",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "draft",
        header: "draft",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "markdown",
        header: "markdown",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "tags",
        header: "tags",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "groups",
        header: "groups",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "updatedAt",
        header: "updatedAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "visits",
        header: "visits",
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "licence",
        header: "licence",
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"articles"} />
    </>
  );
};

export default Articles;
