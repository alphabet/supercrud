import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const ContextSettings = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableEditing: false,
        size: 200,
      },
      {
        accessorFn: (row) => row?.key,
        id: "key",
        header: "key",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.type,
        id: "type",
        header: "type",
        enableEditing: true,
      },
      {
        accessorFn: (row) => row?.value,
        id: "value",
        header: "value",
        enableEditing: true,
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"contextSettings"} />
    </>
  );
};

export default ContextSettings;
