import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Helps = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "title",
        header: "title",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "type",
        header: "type",
      },
      {
        accessorKey: "content",
        header: "content",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "category",
        header: "category",
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"helps"} />
    </>
  );
};

export default Helps;
