import { useMemo } from "react";

import DashBoard from "../components/DashBoard/DashBoard";

const Migrations = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "locked",
        header: "locked",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "version",
        header: "version",
      },
      {
        accessorKey: "lockedAt",
        header: "lockedAt",
        enableEditing: false,
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"migrations"} />
    </>
  );
};

export default Migrations;
