import { useMemo } from "react";

import Dashboard from "../components/DashBoard/DashBoard";

const Skills = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "name",
        header: "name",
      },
      {
        accessorKey: "count",
        header: "count",
        enableGlobalFilter: false,
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
    ],
    []
  );

  return (
    <>
      <Dashboard columns={columns} apiRouteName={"skills"} />
    </>
  );
};

export default Skills;
