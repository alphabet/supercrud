import { useMemo } from "react";

import Dashboard from "../components/DashBoard/DashBoard";

const Roles = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "children",
        header: "children",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <Dashboard columns={columns} apiRouteName={"roles"} />
    </>
  );
};

export default Roles;
