import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Bookmarks = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "url",
        header: "url",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "name",
        header: "name",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "author",
        header: "author",
        size: 200,
        enableClickToCopy: true,
      },
      {
        accessorKey: "groupId",
        header: "groupId",
        size: 200,
        enableEditing: false,
        enableClickToCopy: true,
      },
      {
        accessorKey: "tag",
        header: "tag",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "icon",
        header: "icon",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"bookmarks"} />
    </>
  );
};

export default Bookmarks;
