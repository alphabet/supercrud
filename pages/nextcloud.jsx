import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Nextcloud = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "url",
        header: "url",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "active",
        header: "active",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "count",
        header: "count",
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"nextcloud"} />
    </>
  );
};

export default Nextcloud;
