import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const RoleAssignments = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorFn: (row) => row?.role?._id,
        id: "role._id",
        header: "role.id",
        enableEditing: false,
      },
      {
        accessorKey: "scope",
        header: "scope",
        enableClickToCopy: true,
        enableEditing: false,

        size: 200,
      },
      {
        accessorFn: (row) => row?.user?._id,
        id: "user._id",
        header: "user.id",
        enableClickToCopy: true,
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.inheritedRoles?.[0]._id,
        id: "inheritedroles.[0]._id",
        header: "inheritedroles.id",
        enableClickToCopy: true,
        enableEditing: false,
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"roleAssignments"} />
    </>
  );
};

export default RoleAssignments;
