import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Categories = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "name",
        header: "name",
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"categories"} />
    </>
  );
};

export default Categories;
