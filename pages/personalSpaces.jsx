import { useMemo } from "react";

import DashBoard from "../components/DashBoard/DashBoard";

const PersonalSpaces = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "userId",
        header: "userId",
        size: 200,
        enableClickToCopy: true,
        enableEditing: false,
      },
      {
        accessorKey: "unsorted",
        header: "unsorted",
        size: "260",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (JSON.stringify(cell.getValue())?.length > 1) {
            return JSON.stringify(cell.getValue());
          }
        },
      },
      {
        accessorKey: "sorted",
        header: "sorted",
        size: "840",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (JSON.stringify(cell.getValue())?.length > 1) {
            return JSON.stringify(cell.getValue());
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"personalSpaces"} />
    </>
  );
};

export default PersonalSpaces;
