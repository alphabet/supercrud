import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Users = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorFn: (row) => row?.services?.password?.bcrypt,
        id: "services.password.bcrypt",
        header: "services.password.bcrypt",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ row }) => {
          if (row?.original?.services?.password?.bcrypt?.length > 20) {
            return (
              <details>
                <summary>
                  {row?.original.services.password.bcrypt.substr(0, 12)}
                </summary>
                {row?.original.services.password.bcrypt}
              </details>
            );
          }
        },
      },
      {
        accessorFn: (row) => row?.services?.keycloak?.id,
        id: "services.keycloak.id",
        header: "keycloakId",
        enableEditing: false,
        enableClickToCopy: true,
      },
      {
        accessorKey: "username",
        header: "username",
        size: 260,
      },
      {
        accessorFn: (row) => row?.emails?.[0]?.address,
        id: "emails.[0].address",
        header: "emails.address",
        size: 260,
      },
      {
        accessorFn: (row) => row?.emails?.[0]?.verified,
        id: "emails.[0].verified",
        header: "emails.verified",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "firstName",
        header: "firstName",
      },
      {
        accessorKey: "lastName",
        header: "lastName",
      },
      {
        accessorKey: "structure",
        header: "structure",
        enableClickToCopy: true,
      },
      {
        accessorKey: "isActive",
        header: "isActive",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "isRequest",
        header: "isRequest",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "favServices",
        header: "favServices",
        size: "240",
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "favGroups",
        header: "favGroups",
        size: "240",
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell?.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "favUserBookmarks",
        header: "favUserBookmarks",
        size: "240",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "advancedPersonalPage",
        header: "advancedPersonalPage",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "articlesCount",
        header: "articlesCount",
        enableGlobalFilter: false,
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "groupCount",
        header: "groupCount",
        enableGlobalFilter: false,
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "groupQuota",
        header: "groupQuota",
        enableGlobalFilter: false,
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "nclocator",
        header: "nclocator",
        enableGlobalFilter: false,
      },
      {
        accessorKey: "articlesEnable",
        header: "articlesEnable",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "authToken",
        header: "authToken",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "awaitingStructure",
        header: "awaitingStructure",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "lastLogin",
        header: "lastLogin",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "lastArticle",
        header: "lastArticle",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "heartbeat",
        header: "heartbeat",
        enableEditing: false,
        enableGlobalFilter: false,
      },
      {
        accessorKey: "primaryEmail",
        header: "primaryEmail",
      },
      {
        accessorKey: "language",
        header: "language",
        enableGlobalFilter: false,
      },
      {
        accessorKey: "logoutType",
        header: "logoutType",
        enableGlobalFilter: false,
      },
      {
        accessorKey: "avatar",
        header: "avatar",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "mezigName",
        header: "mezigName",
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"users"} />
    </>
  );
};

export default Users;
