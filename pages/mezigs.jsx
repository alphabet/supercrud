import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Mezigs = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "firstName",
        header: "firstName",
      },
      {
        accessorKey: "lastName",
        header: "lastName",
      },
      {
        accessorKey: "username",
        header: "username",
      },
      {
        accessorKey: "publicName",
        header: "publicName",
      },
      {
        accessorKey: "email",
        header: "email",
        enableEditing: false,
      },
      {
        accessorKey: "blacklist",
        header: "blacklist",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },

      {
        accessorKey: "skills",
        header: "skills",
        size: "240",
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },

      {
        accessorKey: "links",
        header: "links",
        size: "240",
        enableGlobalFilter: false,
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },

      {
        accessorKey: "profileChecked",
        header: "profileChecked",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },

      {
        accessorKey: "biography",
        header: "biography",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "profilPic",
        header: "profilPic",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"mezigs"} />
    </>
  );
};

export default Mezigs;
