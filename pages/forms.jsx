import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Forms = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "title",
        header: "title",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "owner",
        header: "owner",
      },
      {
        accessorKey: "isModel",
        header: "isModel",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "isPublic",
        header: "isPublic",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "editableAnswers",
        header: "editableAnswers",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "groups",
        header: "groups",
        enableEditing: false,
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "active",
        header: "active",
        enableGlobalFilter: false,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"forms"} />
    </>
  );
};

export default Forms;
