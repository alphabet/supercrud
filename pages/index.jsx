import Navigation from "../components/Navigation/Navigation";
import Header from "../components/Header/Header";
import styles from "./index.module.css";
export default function HomePage() {
  return (
    <>
      <Header />
      <h1 className={styles.firstTitle}>Bienvenue</h1>
      <h2 className={styles.secondTitle}>Les tables</h2>
      <div className={styles.navigation}>
        <Navigation />
      </div>
    </>
  );
}
