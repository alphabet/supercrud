import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Groups = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "name",
        header: "name",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "type",
        header: "type",
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "digest",
        header: "digest",
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "content",
        header: "content",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "groupPadID",
        header: "groupPadID",
        enableClickToCopy: true,
        size: 200,
      },
      {
        accessorKey: "owner",
        header: "owner",
        enableClickToCopy: true,
        size: 200,
      },
      {
        accessorKey: "admins",
        header: "admins",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "active",
        header: "active",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "animators",
        header: "animators",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "members",
        header: "members",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "candidates",
        header: "candidates",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "applications",
        header: "applications",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "slug",
        header: "slug",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "avatar",
        header: "avatar",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "numCandidates",
        header: "numCandidates",
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "articles",
        header: "articles",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorFn: (row) => row?.meeting?.attendeePW,
        id: "meeting.attendeePW",
        header: "meeting.attendeePW",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.meeting?.moderatorPW,
        id: "meeting.moderatorPW",
        header: "meeting.moderatorPW",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.meeting?.createTime,
        id: "meeting.createTime",
        header: "meeting.createTime",
        enableEditing: false,
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"groups"} />
    </>
  );
};

export default Groups;
