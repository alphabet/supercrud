import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Analytics = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "content",
        header: "content",
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        enableGlobalFilter: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "structureId",
        header: "structureId",
        enableClickToCopy: true,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "target",
        header: "target",
        enableClickToCopy: true,
      },

      {
        accessorKey: "count",
        header: "count",
        enableEditing: false,
        enableGlobalFilter: false,
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"analytics"} />
    </>
  );
};

export default Analytics;
