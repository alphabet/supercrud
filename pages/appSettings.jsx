import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const AppSettings = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableEditing: false,
        size: 200,
      },

      {
        accessorFn: (row) => row?.introduction?.[0]?.language,
        id: "introduction.[0].language",
        header: "language[0]",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.introduction?.[0]?.content,
        id: "introduction.[0].content",
        header: "content[0]",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.introduction?.[1]?.language,
        id: "introduction.[1].language",
        header: "language[1]",
        enableEditing: false,
      },
      {
        accessorFn: (row) => row?.introduction?.[1]?.content,
        id: "introduction.[1].content",
        header: "content[1]",
        enableEditing: false,
      },

      {
        id: "legal",
        header: "legal",
        muiTableHeadCellProps: () => ({
          sx: {
            color: "red",
            border: "solid 5px red",
          },
        }),
        columns: [
          {
            accessorFn: (row) => row?.legal?.external,
            id: "legal.external",
            header: "external",
            enableEditing: false,
            Cell: ({ cell }) => cell.getValue()?.toString(),
          },
          {
            accessorFn: (row) => row?.legal?.link,
            id: "legal.link",
            header: "link",
            enableEditing: false,
          },
          {
            accessorFn: (row) => row?.legal?.content,
            id: "legal.content",
            header: "content",
            enableEditing: false,
          },
        ],
      },
      {
        id: "accessibility",
        header: "accessibility",
        muiTableHeadCellProps: () => ({
          sx: {
            color: "red",
            border: "solid 5px red",
          },
        }),
        columns: [
          {
            accessorFn: (row) => row?.accessibility?.external,
            id: "accessibility.external",
            header: "external",
            enableEditing: false,
            Cell: ({ cell }) => cell.getValue()?.toString(),
          },
          {
            accessorFn: (row) => row?.accessibility?.link,
            id: "accessibility.link",
            header: "link",
            enableEditing: false,
          },
          {
            accessorFn: (row) => row?.accessibility?.content,
            id: "accessibility.content",
            header: "content",
            enableEditing: false,
          },
        ],
      },
      {
        id: "gcu",
        header: "gcu",
        muiTableHeadCellProps: () => ({
          sx: {
            color: "red",
            border: "solid 5px red",
          },
        }),
        columns: [
          {
            accessorFn: (row) => row?.gcu?.external,
            id: "gcu.external",
            header: "external",
            enableEditing: false,
            Cell: ({ cell }) => cell.getValue()?.toString(),
          },
          {
            accessorFn: (row) => row?.gcu?.link,
            id: "gcu.link",
            header: "link",
            enableEditing: false,
          },
          {
            accessorFn: (row) => row?.gcu?.content,
            id: "gcu.content",
            header: "content",
            enableEditing: false,
          },
        ],
      },
      {
        id: "personalData",
        header: "personalData",
        muiTableHeadCellProps: () => ({
          sx: {
            color: "red",
            border: "solid 5px red",
          },
        }),
        columns: [
          {
            accessorFn: (row) => row?.personalData?.external,
            id: "personalData.external",
            header: "external",
            enableEditing: false,
            Cell: ({ cell }) => cell.getValue()?.toString(),
          },
          {
            accessorFn: (row) => row?.personalData?.link,
            id: "personalData.link",
            header: "link",
            enableEditing: false,
          },
          {
            accessorFn: (row) => row?.personalData?.content,
            id: "personalData.content",
            header: "content",
            enableEditing: false,
          },
        ],
      },
      {
        accessorKey: "maintenance",
        header: "maintenance",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "textMaintenance",
        header: "textMaintenance",
      },
      {
        accessorKey: "userStructureValidationMandatory",
        header: "userStructureValidationMandatory",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"appSettings"} />
    </>
  );
};

export default AppSettings;
