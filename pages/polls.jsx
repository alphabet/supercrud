import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Polls = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "title",
        header: "title",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "groups",
        header: "groups",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "duration",
        header: "duration",
        enableEditing: false,
      },
      {
        accessorKey: "public",
        header: "public",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "allDay",
        header: "allDay",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "type",
        header: "type",
      },
      {
        accessorKey: "active",
        header: "active",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "completed",
        header: "completed",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "userId",
        header: "userId",
        size: 200,
        enableClickToCopy: true,
        enableEditing: false,
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "updatedAt",
        header: "updatedAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"polls"} />
    </>
  );
};

export default Polls;
