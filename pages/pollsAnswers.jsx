import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const PollsAnswers = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "email",
        header: "email",
        enableClickToCopy: true,
      },
      {
        accessorKey: "pollId",
        header: "pollId",
        size: 200,
        enableClickToCopy: true,
        enableEditing: false,
      },
      {
        accessorKey: "confirmed",
        header: "confirmed",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "name",
        header: "name",
      },
      {
        accessorKey: "userId",
        header: "userId",
        size: 200,
        enableEditing: false,
        enableClickToCopy: true,
      },
      {
        accessorKey: "updatedAt",
        header: "updatedAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"pollsAnswers"} />
    </>
  );
};

export default PollsAnswers;
