import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Services = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableClickToCopy: true,
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "url",
        header: "url",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "logo",
        header: "logo",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "title",
        header: "title",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "state",
        header: "state",
        muiTableBodyCellEditTextFieldProps: () => ({
          type: "number",
        }),
      },
      {
        accessorKey: "description",
        header: "description",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "categories",
        header: "categories",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "team",
        header: "team",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "usage",
        header: "usage",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "content",
        header: "content",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "screenshots",
        header: "screenshots",
        enableEditing: false,
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "slug",
        header: "slug",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "structure",
        header: "structure",
        enableClickToCopy: true,
      },
      {
        accessorKey: "offline",
        header: "offline",
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"services"} />
    </>
  );
};

export default Services;
