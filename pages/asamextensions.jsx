import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const AsamExtensions = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "extension",
        header: "extension",
      },
      {
        accessorKey: "entiteNomCourt",
        header: "entiteNomCourt",
      },
      {
        accessorKey: "entiteNomLong",
        header: "entiteNomLong",
      },
      {
        accessorKey: "familleNomCourt",
        header: "familleNomCourt",
      },
      {
        accessorKey: "familleNomLong",
        header: "familleNomLong",
      },
      {
        accessorKey: "structureId",
        header: "structureId",
        enableClickToCopy: true,
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"asamextensions"} />
    </>
  );
};

export default AsamExtensions;
