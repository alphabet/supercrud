import { getServerSession } from "next-auth/next";
import { authOptions } from "../auth/[...nextauth]";

import connectDB from "../../../database/connectDB";
import UserBookmarks from "../../../database/models/UserBookmarks";

export default async function handler(req, res) {
  const session = await getServerSession(req, res, authOptions);
  const { method } = req;
  const { tableName } = req.query;

  if (!session)
    return res.end(
      "You must be signed in to view the protected content on this page."
    );

  if (!tableName) return res.end("Cannot access API by URL");

  await connectDB();

  switch (method) {
    case "GET":
      try {
        const userBookmarks = await UserBookmarks.find({});
        res.status(200).json({ data: userBookmarks });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
    case "DELETE":
      try {
        const bodyParsed = JSON.parse(req.body);
        const id = bodyParsed.id;
        const result = await UserBookmarks.findOneAndDelete({ _id: id });
        res.status(200).json({
          success: true,
          result,
          message: "supprimé de la Base de Donnée !",
        });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
    case "PUT":
      try {
        const bodyParsed = JSON.parse(req.body);
        const id = bodyParsed._id;
        const result = await UserBookmarks.findByIdAndUpdate(
          { _id: id },
          { ...bodyParsed }
        );
        res.status(200).json({
          success: true,
          result,
          message: "Modifié en Base de Donnée !",
        });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
  }
}
