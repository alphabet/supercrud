import NextAuth from "next-auth";
import KeycloakProvider from "next-auth/providers/keycloak";

import { serverRuntimeConfig } from "../../../utils/envConfig";

export const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: serverRuntimeConfig.KEYCLOAK_ID,
      clientSecret: serverRuntimeConfig.KEYCLOAK_SECRET,
      issuer: serverRuntimeConfig.KEYCLOAK_ISSUER,
    }),
  ],
};

export default NextAuth(authOptions);
