import { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const GlobalInfos = () => {
  const columns = useMemo(
    () => [
      {
        accessorKey: "_id",
        header: "id",
        enableEditing: false,
        size: 200,
      },
      {
        accessorKey: "language",
        header: "language",
      },
      {
        accessorKey: "content",
        header: "content",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "expirationDate",
        header: "expirationDate",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "createdAt",
        header: "createdAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "updatedAt",
        header: "updatedAt",
        enableEditing: false,
        Cell: ({ cell }) => {
          if (cell.getValue()) {
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "structureId",
        header: "structureId",
        enableEditing: false,
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"globalInfos"} />
    </>
  );
};

export default GlobalInfos;
