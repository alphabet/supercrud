/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
};

module.exports = nextConfig;

module.exports = {
  output: "standalone",
  serverRuntimeConfig: {
    // Will only be available on the server side
    MONGODB_URI: process.env.MONGODB_URI,
    KEYCLOAK_SECRET: process.env.KEYCLOAK_SECRET,
    KEYCLOAK_ID: process.env.KEYCLOAK_ID,
    KEYCLOAK_ISSUER: process.env.KEYCLOAK_ISSUER,
    NEXTAUTH_URL: process.env.NEXTAUTH_URL,
    NEXTAUTH_SECRET: process.env.NEXTAUTH_SECRET,
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    NEXT_PUBLIC_AUTHORIZED: process.env.NEXT_PUBLIC_AUTHORIZED,
    NEXT_PUBLIC_EDITING_ACCESS: process.env.NEXT_PUBLIC_EDITING_ACCESS,
    NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL,
  },
};

// LOG SERVER
const charactere = "-".repeat(20);
const text = "SETTINGS LOADED FROM ENV";
const logEnv = {
  MONGODB_URI: process.env.MONGODB_URI,
  KEYCLOAK_SECRET: process.env.KEYCLOAK_SECRET,
  KEYCLOAK_ID: process.env.KEYCLOAK_ID,
  KEYCLOAK_ISSUER: process.env.KEYCLOAK_ISSUER,
  NEXTAUTH_URL: process.env.NEXTAUTH_URL,
  NEXTAUTH_SECRET: process.env.NEXTAUTH_SECRET,
  NEXT_PUBLIC_AUTHORIZED: process.env.NEXT_PUBLIC_AUTHORIZED,
  NEXT_PUBLIC_EDITING_ACCESS: process.env.NEXT_PUBLIC_EDITING_ACCESS,
  NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL,
};
console.log(" ");
console.log(`${charactere} ${text} ${charactere}`);
console.log(" ");
console.table(logEnv);
console.log(" ");
console.log(`${charactere}${charactere}${charactere}${charactere}`);
