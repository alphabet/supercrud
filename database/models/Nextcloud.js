import mongoose from "mongoose";

const NextcloudSchema = new mongoose.Schema(
  {
    _id: { type: String },
    url: { type: String },
    active: { type: Boolean },
    count: { type: Number },
  },
  { collection: "nextcloud" },
  { timestamps: true }
);

export default mongoose.models.Nextcloud ||
  mongoose.model("Nextcloud", NextcloudSchema);
