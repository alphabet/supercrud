import mongoose from "mongoose";

const TagsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    name: { type: String },
  },
  { collection: "tags" },
  { timestamps: true }
);

export default mongoose.models.Tags || mongoose.model("Tags", TagsSchema);
