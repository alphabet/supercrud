import mongoose from "mongoose";

const DefaultSpacesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    structureId: { type: String },
    updatedAt: { type: Date },
  },
  { collection: "defaultspaces" },
  { timestamps: true }
);

export default mongoose.models.DefaultSpaces ||
  mongoose.model("DefaultSpaces", DefaultSpacesSchema);
