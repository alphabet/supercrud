import mongoose from "mongoose";

const BookmarksSchema = new mongoose.Schema(
  {
    _id: { type: String },
    url: { type: String },
    name: { type: String },
    author: { type: String },
    groupId: { type: String },
    tag: { type: String },
    icon: { type: String },
  },
  { collection: "bookmarks" },
  { timestamps: true }
);

export default mongoose.models.Bookmarks ||
  mongoose.model("Bookmarks", BookmarksSchema);
