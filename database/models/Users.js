import mongoose from "mongoose";

const UsersSchema = new mongoose.Schema(
  {
    _id: { type: String },
    createdAt: { type: Date },
    services: { type: Object },
    username: { type: String },
    emails: { type: Array },
    firstName: { type: String },
    lastName: { type: String },
    isActive: { type: Boolean },
    isRequest: { type: Boolean },
    favServices: [String],
    favGroups: [String],
    favUserBookmarks: [String],
    advancedPersonalPage: { type: Boolean },
    articlesCount: { type: Number },
    groupCount: { type: Number },
    groupQuota: { type: Number },
    nclocator: { type: String },
    articlesEnable: { type: Boolean },
    authToken: { type: String },
    lastLogin: { type: Date },
    profile: { type: Object },
    heartbeat: { type: Date },
    awaitingStructure: { type: Boolean },
    primaryEmail: { type: String },
    language: { type: String },
    logoutType: { type: String },
    lastArticle: { type: Date },
    avatar: { type: String },
    mezigName: { type: String },
    structure: { type: String },
  },
  { collection: "users" },
  { timestamps: true }
);

export default mongoose.models.Users || mongoose.model("Users", UsersSchema);
