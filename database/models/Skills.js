import mongoose from "mongoose";

const SkillsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    name: { type: String },
    count: { type: Number },
  },
  { collection: "skills" },
  { timestamps: true }
);

export default mongoose.models.Skills || mongoose.model("Skills", SkillsSchema);
