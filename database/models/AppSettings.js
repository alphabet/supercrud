import mongoose from "mongoose";

const AppSettingsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    introduction: [Object],
    legal: { type: Object },
    accessibility: { type: Object },
    gcu: { type: Object },
    personalData: { type: Object },
    maintenance: { type: Boolean },
    textMaintenance: { type: String },
    userStructureValidationMandatory: { type: Boolean },
  },
  { collection: "appsettings" },
  { timestamps: true }
);

export default mongoose.models.AppSettings ||
  mongoose.model("AppSettings", AppSettingsSchema);
