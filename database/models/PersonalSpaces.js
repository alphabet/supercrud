import mongoose from "mongoose";

const PersonalSpacesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    userId: { type: String },
    unsorted: [Object],
    sorted: [Object],
  },
  { collection: "personalspaces" },
  { timestamps: true }
);

export default mongoose.models.PersonalSpaces ||
  mongoose.model("PersonalSpaces", PersonalSpacesSchema);
