import mongoose from "mongoose";

const RolesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    children: [String],
  },
  { collection: "roles" },
  { timestamps: true }
);

export default mongoose.models.Roles || mongoose.model("Roles", RolesSchema);
