import mongoose from "mongoose";

const AnalyticsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    content: { type: String },
    createdAt: { type: Date },
    structureId: { type: String },
    target: { type: String },
    count: { type: Number },
  },
  { collection: "analytics" },
  { timestamps: true }
);

export default mongoose.models.Analytics ||
  mongoose.model("Analytics", AnalyticsSchema);
