import mongoose from "mongoose";

const GlobalInfos = new mongoose.Schema(
  {
    _id: { type: String },
    language: { type: String },
    content: { type: String },
    expirationDate: { type: Date },
    createdAt: { type: Date },
    updatedAt: { type: Date },
    structureId: [String],
  },
  { collection: "globalinfos" },
  { timestamps: true }
);

export default mongoose.models.GlobalInfos ||
  mongoose.model("GlobalInfos", GlobalInfos);
