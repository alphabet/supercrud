import mongoose from "mongoose";

const HelpsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    title: { type: String },
    description: { type: String },
    type: { type: Number },
    content: { type: String },
    category: { type: String },
  },
  { collection: "helps" },
  { timestamps: true }
);

export default mongoose.models.Helps || mongoose.model("Helps", HelpsSchema);
