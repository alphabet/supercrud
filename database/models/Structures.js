import mongoose from "mongoose";

const StructuresSchema = new mongoose.Schema(
  {
    _id: { type: String },
    name: { type: String },
    parentId: { type: String },
    childrenIds: [String],
    ancestorsIds: [String],
    introduction: [Object],
    contactEmail: { type: String },
  },
  { collection: "structures" },
  { timestamps: true }
);

export default mongoose.models.Structures ||
  mongoose.model("Structures", StructuresSchema);
