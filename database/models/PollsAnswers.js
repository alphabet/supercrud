import mongoose from "mongoose";

const Polls_AnswersSchema = new mongoose.Schema(
  {
    _id: { type: String },
    email: { type: String },
    pollId: { type: String },
    choices: [Object],
    confirmed: { type: Boolean },
    createdAt: { type: Date },
    meetingSlot: [Object],
    name: { type: String },
    updatedAt: { type: Date },
  },
  { collection: "polls_answers" },
  { timestamps: true }
);

export default mongoose.models.Polls_Answers ||
  mongoose.model("Polls_Answers", Polls_AnswersSchema);
