import mongoose from "mongoose";

const CategoriesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    name: { type: String },
  },
  { collection: "categories" },
  { timestamps: true }
);

export default mongoose.models.Categories ||
  mongoose.model("Categories", CategoriesSchema);
