import mongoose from "mongoose";

const Forms = new mongoose.Schema(
  {
    _id: { type: String },
    title: { type: String },
    description: { type: String },
    owner: { type: String },
    isModel: { type: Boolean },
    isPublic: { type: Boolean },
    editableAnswers: { type: Boolean },
    groups: [String],
    components: { type: Array },
    active: { type: Boolean },
    createdAt: { type: Date },
  },
  { collection: "forms" },
  { timestamps: true }
);

export default mongoose.models.Forms || mongoose.model("Forms", Forms);
