import mongoose from "mongoose";

const AsamExtensions = new mongoose.Schema(
  {
    _id: { type: String },
    extension: { type: String },
    entiteNomCourt: { type: String },
    entiteNomLong: { type: String },
    familleNomCourt: { type: String },
    familleNomLong: { type: String },
    structureId: { type: String },
  },
  { collection: "asamextensions" },
  { timestamps: true }
);

export default mongoose.models.AsamExtensions ||
  mongoose.model("AsamExtensions", AsamExtensions);
