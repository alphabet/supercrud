import mongoose from "mongoose";

const RoleAssignmentsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    role: { type: Object },
    scope: { type: String },
    user: { type: Object },
    inheritedRoles: [Object],
  },
  { collection: "role-assignment" },
  { timestamps: true }
);

export default mongoose.models.RoleAssignments ||
  mongoose.model("RoleAssignments", RoleAssignmentsSchema);
