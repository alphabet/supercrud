import mongoose from "mongoose";

const ServicesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    url: { type: String },
    logo: { type: String },
    title: { type: String },
    state: { type: Number },
    description: { type: String },
    categories: [String],
    team: { type: String },
    usage: { type: String },
    content: { type: String },
    screenshots: [String],
    structure: { type: String },
    slug: { type: String },
    offline: { type: Boolean },
  },
  { collection: "services" },
  { timestamps: true }
);

export default mongoose.models.Services ||
  mongoose.model("Services", ServicesSchema);
