import mongoose from "mongoose";

const MezigsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    username: { type: String },
    publicName: { type: String },
    blacklist: { type: Boolean },
    skills: [String],
    links: [String],
    profileChecked: { type: Boolean },
    biography: { type: String },
    email: { type: String },
    profilPic: { type: String },
  },
  { collection: "mezigs" },
  { timestamps: true }
);

export default mongoose.models.Mezigs || mongoose.model("Mezigs", MezigsSchema);
