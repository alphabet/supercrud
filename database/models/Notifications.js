import mongoose from "mongoose";

const NotificationsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    userId: { type: String },
    title: { type: String },
    content: { type: String },
    type: { type: String },
    link: { type: String },
    read: { type: Boolean },
    createdAt: { type: Date },
    expireAt: { type: Date },
  },
  { collection: "notifications" },
  { timestamps: true }
);

export default mongoose.models.Notifications ||
  mongoose.model("Notifications", NotificationsSchema);
