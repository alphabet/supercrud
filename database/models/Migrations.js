import mongoose from "mongoose";

const MigrationsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    locked: { type: Boolean },
    version: { type: Number },
    lockedAt: { type: Date },
  },
  { collection: "migrations" },
  { timestamps: true }
);

export default mongoose.models.Migrations ||
  mongoose.model("Migrations", MigrationsSchema);
