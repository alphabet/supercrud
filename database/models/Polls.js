import mongoose from "mongoose";

const PollsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    dates: [Object],
    title: { type: String },
    description: { type: String },
    groups: [String],
    meetingSlots: [Object],
    duration: { type: String },
    public: { type: Boolean },
    allDay: { type: Boolean },
    type: { type: String },
    active: { type: Boolean },
    completed: { type: Boolean },
    userId: { type: String },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  { collection: "polls" },
  { timestamps: true }
);

export default mongoose.models.Polls || mongoose.model("Polls", PollsSchema);
