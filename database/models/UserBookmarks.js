import mongoose from "mongoose";

const UserBookmarksSchema = new mongoose.Schema(
  {
    _id: { type: String },
    url: { type: String },
    name: { type: String },
    userId: { type: String },
    tag: { type: String },
    icon: { type: String },
  },
  { collection: "userBookmarks" },
  { timestamps: true }
);

export default mongoose.models.UserBookmarks ||
  mongoose.model("UserBookmarks", UserBookmarksSchema);
