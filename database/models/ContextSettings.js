import mongoose from "mongoose";

const ContextSettingsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    key: { type: String },
    type: { type: String },
    value: { type: Array },
    'value.$': {
      type: { any: {} },
    }
  },
  { collection: "contextsettings" },
  { timestamps: true }
);

export default mongoose.models.ContextSettings ||
  mongoose.model("ContextSettings", ContextSettingsSchema);
