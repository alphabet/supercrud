import mongoose from "mongoose";

const ArticlesSchema = new mongoose.Schema(
  {
    _id: { type: String },
    title: { type: String },
    content: { type: String },
    description: { type: String },
    licence: { type: String },
    tags: [String],
    markdown: { type: Boolean },
    draft: { type: Boolean },
    groups: [String],
    visits: { type: Number },
    userId: { type: String },
    structure: { type: String },
    slug: { type: String },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  { collection: "articles" },
  { timestamps: true }
);

export default mongoose.models.Articles ||
  mongoose.model("Articles", ArticlesSchema);
