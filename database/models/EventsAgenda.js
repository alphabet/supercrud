import mongoose from "mongoose";

const EventsAgendaSchema = new mongoose.Schema(
  {
    _id: { type: String },
    title: { type: String },
    location: { type: String },
    allDay: { type: Boolean },
    recurrent: { type: Boolean },
    guests: [String],
    groups: [String],
    participants: [String],
    daysOfWeek: { type: Boolean },
    start: { type: Date },
    end: { type: Date },
    startTime: { type: Date },
    endTime: { type: Date },
    userId: { type: String },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  { collection: "eventsAgenda" },
  { timestamps: true }
);

export default mongoose.models.EventsAgenda ||
  mongoose.model("EventsAgenda", EventsAgendaSchema);
