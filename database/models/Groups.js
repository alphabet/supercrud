import mongoose from "mongoose";

const GroupsSchema = new mongoose.Schema(
  {
    _id: { type: String },
    owner: { type: String },
    name: { type: String },
    type: { type: Number },
    description: { type: String },
    content: { type: String },
    groupPadID: { type: String },
    digest: { type: String },
    admins: [String],
    active: { type: Boolean },
    animators: [String],
    members: [String],
    candidates: [String],
    applications: [String],
    slug: { type: String },
    avatar: { type: String },
    numCandidates: { type: Number },
    plugins: { type: Object },
    articles: { type: Boolean },
    meeting: { type: Object },
  },
  { collection: "groups" },
  { timestamps: true }
);

export default mongoose.models.Groups || mongoose.model("Groups", GroupsSchema);
