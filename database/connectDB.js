import mongoose from "mongoose";

import { serverRuntimeConfig } from "../utils/envConfig";

const MONGODB_URI = serverRuntimeConfig.MONGODB_URI;

if (!MONGODB_URI) {
  throw new Error(
    "Please define the MONGODB_URI environment variable inside .env.local"
  );
}

/**
 * Global is used here to maintain a cached connection across hot reloads
 * in development. This prevents connections growing exponentially
 * during API Route usage.
 */
let cached = global.mongoose;

if (!cached) {
  cached = global.mongoose = { conn: null, promise: null };
}

async function connectDB() {
  if (cached.conn) {
    console.log(`DB connected in cache , URI : ${MONGODB_URI}`);
    return cached.conn;
  }

  if (!cached.promise) {
    const opts = {
      bufferCommands: false,
    };

    cached.promise = mongoose.connect(MONGODB_URI, opts).then((mongoose) => {
      console.log(`DB connected in new connection, URI : ${MONGODB_URI}`);
      return mongoose;
    });
  }

  try {
    cached.conn = await cached.promise;
  } catch (error) {
    cached.promise = null;
    throw error;
  }

  return cached.conn;
}
// For remove cached connection
// async function connectDB() {
//   return mongoose.connect(`${MONGODB_URI}`);
//   // let test = await mongoose.connect(`${MONGODB_URI}`);
//   // console.log(test.modelNames());
//return test
// }

export default connectDB;
