import { useEffect, useState } from "react";
import Logout from "../Logout/Logout";
import { useSession } from "next-auth/react";
import styles from "./Header.module.css";
import { publicRuntimeConfig } from "../../utils/envConfig";

export default function Header({ tableName }) {
  const { data: session } = useSession();
  const [permissions, setPermissions] = useState("LECTURE SEULE");

  useEffect(() => {
    if (
      publicRuntimeConfig.NEXT_PUBLIC_EDITING_ACCESS.includes(
        session.user.email
      )
    )
      setPermissions("EDITION");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className={styles.header}>
        <p>
          Signed in as {session.user.email} avec les droits de : {permissions}
        </p>
        {tableName ? (
          <h2 className={styles.title}>{`Table : ${tableName}`}</h2>
        ) : null}

        <Logout />
      </div>
    </>
  );
}
