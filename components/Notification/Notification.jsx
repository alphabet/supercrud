import { Alert, Snackbar } from "@mui/material";

export default function Notification({ allProps }) {
  const {
    openAlert,
    setOpenAlert,
    alertMessage,
    setAlertMessage,
    alertSeverity,
    setAlertSeverity,
  } = allProps;

  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      open={openAlert}
      autoHideDuration={5000}
      onClose={() => {
        setOpenAlert(false);
        setAlertMessage("");
        setAlertSeverity("");
      }}
    >
      <Alert
        onClose={() => {
          setOpenAlert(false);
          setAlertMessage("");
          setAlertSeverity("");
        }}
        severity={alertSeverity}
        variant="filled"
        sx={{ width: "100%" }}
      >
        {alertMessage}
      </Alert>
    </Snackbar>
  );
}
