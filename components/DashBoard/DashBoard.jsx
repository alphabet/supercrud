import { useEffect, useState, useRef, useCallback } from "react";
import MaterialReactTable from "material-react-table";
import { useSession } from "next-auth/react";

import { Box, IconButton, Tooltip } from "@mui/material";
import { Delete, Edit } from "@mui/icons-material";

import styles from "./DashBoard.module.css";

import Navigation from "../Navigation/Navigation";
import Header from "../Header/Header";
import Notification from "../Notification/Notification";

import { formatData } from "../../utils/table/formatData";
import { publicRuntimeConfig } from "../../utils/envConfig";

const DashBoard = ({ columns, apiRouteName }) => {
  const [tableData, setTableData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [sorting, setSorting] = useState([]);
  const [dataToSend, setDataToSend] = useState("");

  const [openAlert, setOpenAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [alertSeverity, setAlertSeverity] = useState("");

  const { data: session } = useSession();

  const virtualizerInstanceRef = useRef(null);

  const url = new URL(
    `${publicRuntimeConfig.NEXT_PUBLIC_BASE_URL}/api/${apiRouteName}`
  );
  url.searchParams.set("tableName", apiRouteName); // for block api acces by url

  const formatDataToSend = formatData(tableData, setDataToSend);

  async function fetchData() {
    try {
      const response = await fetch(url.href);
      const json = await response.json();
      setTableData(json.data);
      setIsLoading(false);
    } catch (error) {
      setAlertMessage(`${error}`);
      setAlertSeverity("error");
      setOpenAlert(true);
      return;
    }
  }

  async function updateData() {
    try {
      const response = await fetch(url.href, {
        method: "PUT",
        body: JSON.stringify(dataToSend),
      });
      const json = await response.json();
      setAlertMessage(`${json.result._id} ${json.message}`);
      setAlertSeverity("success");
      setOpenAlert(true);
    } catch (error) {
      setAlertMessage(`${error}`);
      setAlertSeverity("error");
      setOpenAlert(true);
      return;
    }
  }

  const closeModal = async ({ exitEditingMode }) => {
    if (dataToSend) {
      try {
        await updateData(dataToSend);
        exitEditingMode();
        setDataToSend("");
      } catch (error) {
        setAlertMessage(`${error}`);
        setAlertSeverity("error");
        setOpenAlert(true);
      }
    }
    fetchData();
  };

  const deleteData = useCallback(async (row) => {
    if (!confirm(`Are you sure you want to delete ${row.getValue("_id")}`)) {
      return;
    }
    try {
      const response = await fetch(url.href, {
        method: "DELETE",
        body: JSON.stringify({ id: row.getValue("_id") }),
      });
      const json = await response.json();
      setAlertMessage(`${json.result._id} ${json.message}`);
      setAlertSeverity("success");
      setOpenAlert(true);
      fetchData();
    } catch (error) {
      setAlertMessage(`${error}`);
      setAlertSeverity("error");
      setOpenAlert(true);
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [apiRouteName]);

  //scroll to the top of the table when the sorting changes
  useEffect(() => {
    if (virtualizerInstanceRef.current) {
      virtualizerInstanceRef.current.scrollToIndex(0);
    }
  }, [sorting]);

  if (!session) return <p>Pas de session</p>;

  return (
    <>
      {openAlert ? (
        <Notification
          allProps={{
            openAlert,
            setOpenAlert,
            alertMessage,
            setAlertMessage,
            alertSeverity,
            setAlertSeverity,
          }}
        />
      ) : null}
      <Header tableName={apiRouteName} />
      <div className={styles.navigation}>
        <Navigation />
      </div>
      <MaterialReactTable
        columns={columns ?? []}
        data={tableData ?? []}
        enableBottomToolbar={false}
        enablePagination={false}
        enableRowNumbers
        rowNumberMode="static"
        enableRowVirtualization
        enableStickyHeader
        enableEditing={
          !!publicRuntimeConfig.NEXT_PUBLIC_EDITING_ACCESS.includes(
            session.user.email
          )
        }
        state={{ isLoading, sorting }}
        rowVirtualizerInstanceRef={virtualizerInstanceRef}
        rowVirtualizerProps={{ overscan: 20 }}
        initialState={{
          showColumnFilters: false,
          density: "compact",
        }}
        onSortingChange={setSorting}
        onEditingRowSave={closeModal}
        muiTableBodyCellEditTextFieldProps={({ cell }) => ({
          onBlur: (event) => {
            formatDataToSend(cell, event.target.value);
          },
        })}
        muiSearchTextFieldProps={{
          placeholder: "Search into all fields",
          sx: { minWidth: "300px" },
        }}
        muiTablePaperProps={{
          elevation: 0,
        }}
        muiTableContainerProps={{
          sx: {
            maxHeight: "74vh",
            maxWidth: "90vw",
            margin: "auto",
            borderRight: "1px solid rgba(25, 118, 210, 0.5)",
            borderLeft: "1px solid rgba(25, 118, 210, 0.5)",
            borderBottom: "1px solid rgba(25, 118, 210, 0.5)",
            borderRadius: "15px",
          },
        }}
        muiTableHeadCellProps={{
          align: "center",
          sx: {
            borderRight: "solid 1px rgba(25, 118, 210, 0.5)",
            borderLeft: "solid 1px rgba(25, 118, 210, 0.5)",
            borderTop: "solid 1px rgba(25, 118, 210, 0.5)",
            borderRadius: "15px 15px 0 0",
            color: "black",
            fontSize: "15px",
          },
        }}
        muiTableBodyCellProps={{
          sx: {
            color: "black",
            textAlign: "center",
            fontSize: "15px",
          },
        }}
        muiTableBodyProps={{
          sx: {
            //stripe the rows, make odd rows a darker color
            "& tr:nth-of-type(odd)": {
              backgroundColor: "rgba(25, 118, 210, 0.1)",
            },
          },
        }}
        muiTopToolbarProps={{
          sx: {
            marginRight: "4vw",
          },
        }}
        displayColumnDefOptions={{
          "mrt-row-actions": {
            muiTableHeadCellProps: {
              align: "center",
            },
            size: 100,
          },
        }}
        renderRowActions={({ row, table }) => (
          <Box sx={{ display: "flex", width: "s%" }}>
            <Tooltip arrow placement="left" title="Edit">
              <IconButton onClick={() => table.setEditingRow(row)}>
                <Edit />
              </IconButton>
            </Tooltip>
            <Tooltip arrow placement="right" title="Delete">
              <IconButton color="error" onClick={() => deleteData(row)}>
                <Delete />
              </IconButton>
            </Tooltip>
          </Box>
        )}
      />
    </>
  );
};
export default DashBoard;
