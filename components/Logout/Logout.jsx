import { signOut } from "next-auth/react";
import styles from "./Logout.module.css";

export default function Logout() {
  return (
    <button className={styles.logout} onClick={() => signOut()}>
      Logout
    </button>
  );
}
