import Link from "next/link";
import styles from "./Navigation.module.css";

export default function Navigation() {
  return (
    <>
      <div className={styles.link}>
        <Link href="/appSettings">AppSettings</Link>
      </div>
      <div className={styles.link}>
        <Link href="/analytics">Analytics</Link>
      </div>
      <div className={styles.link}>
        <Link href="/articles">Articles</Link>
      </div>
      <div className={styles.link}>
        <Link href="/asamextensions">AsamExtensions</Link>
      </div>
      <div className={styles.link}>
        <Link href="/bookmarks">Bookmarks</Link>
      </div>
      <div className={styles.link}>
        <Link href="/categories">Catégories</Link>
      </div>
      <div className={styles.link}>
        <Link href="/contextSettings">ContextSettings</Link>
      </div>
      <div className={styles.link}>
        <Link href="/defaultSpaces">DefaultSpaces</Link>
      </div>
      <div className={styles.link}>
        <Link href="/forms">Forms</Link>
      </div>
      <div className={styles.link}>
        <Link href="/eventsAgenda">EventsAgenda</Link>
      </div>
      <div className={styles.link}>
        <Link href="/globalInfos">GlobalInfos</Link>
      </div>
      <div className={styles.link}>
        <Link href="/groups">Groups</Link>
      </div>
      <div className={styles.link}>
        <Link href="/helps">Helps</Link>
      </div>
      <div className={styles.link}>
        <Link href="/mezigs">Mezigs</Link>
      </div>
      <div className={styles.link}>
        <Link href="/migrations">Migrations</Link>
      </div>
      <div className={styles.link}>
        <Link href="/nextcloud">Nextcloud</Link>
      </div>
      <div className={styles.link}>
        <Link href="/notifications">Notifications</Link>
      </div>
      <div className={styles.link}>
        <Link href="/personalSpaces">PersonalSpaces</Link>
      </div>
      <div className={styles.link}>
        <Link href="/polls">Polls</Link>
      </div>
      <div className={styles.link}>
        <Link href="/pollsAnswers">pollsAnswers</Link>
      </div>
      <div className={styles.link}>
        <Link href="/roles">Roles</Link>
      </div>
      <div className={styles.link}>
        <Link href="/roleAssignments">RoleAssignment</Link>
      </div>
      <div className={styles.link}>
        <Link href="/services">Services</Link>
      </div>
      <div className={styles.link}>
        <Link href="/skills">Skills</Link>
      </div>
      <div className={styles.link}>
        <Link href="/structures">Structures</Link>
      </div>
      <div className={styles.link}>
        <Link href="/tags">Tags</Link>
      </div>
      <div className={styles.link}>
        <Link href="/users">Users</Link>
      </div>
      <div className={styles.link}>
        <Link href="/userBookmarks">UserBookmarks</Link>
      </div>
    </>
  );
}
