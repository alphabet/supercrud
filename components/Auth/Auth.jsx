import Logout from "../Logout/Logout";
import { useSession } from "next-auth/react";
import styles from "./Auth.module.css";
import { publicRuntimeConfig } from "../../utils/envConfig";

export default function Auth({ children }) {
  const { data: session, status } = useSession({ required: true });

  if (status === "loading") return <div>Loading authentification...</div>;

  if (
    status === "authenticated" &&
    !publicRuntimeConfig.NEXT_PUBLIC_AUTHORIZED.includes(session.user.email)
  ) {
    return (
      <div className={styles.auth}>
        <p>Désolé {session.user.email}, vous n avez pas les droits</p>
        <Logout />
      </div>
    );
  }
  return children;
}
