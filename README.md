# SUPERCRUD V2

> Application de visualisation de Base De Données concue pour LABOITE

# SETUP

```bash
git clone
npm install
```

> Creer un fichier .env a la racine du projet , en vous appuyant sur le .env.sample

```bash

SERVER SIDE ENV

MONGODB_URI='URI de votre base de donnée'
KEYCLOAK_SECRET='KEYCLOAK CLIENT SECRET'
KEYCLOAK_ID='KEYCLOAK CLIENT ID'
KEYCLOAK_ISSUER='URL complète avec le realm'
NEXTAUTH_URL='URL de votre site'
NEXTAUTH_SECRET='Un code secret de votre choix pour décrypter les tokens de sessions'


CLIENT SIDE ENV

NEXT_PUBLIC_AUTHORIZED='Tableau d email autorisé a utiliser cette application'
NEXT_PUBLIC_EDITING_ACCESS='Tableau d email autorisé pour editer la base de donnée'
NEXT_PUBLIC_BASE_URL="URL de votre site"
```

Lancer ensuite la commande

```bash
npm run dev
```

Vous pouvez tester votre application environnement "buildé" pour profiter de la compilation de l'app

```bash
npm run build
npm run start
```

# DEPLOIEMENT

1. https://raphaelpralat.medium.com/system-environment-variables-in-next-js-with-docker-1f0754e04cde

commande pour generer le conteneur docker

```bash
sudo docker build -t supercrud .
```

commande pour lancer l app en docker

```bash
sudo docker run --env-file .env.test -p 3080:3000 supercrud
```

le .env.test est le fichier contenant vos variables d environnement et qui sera utilisé lors du run . Ce fichier remplacera le .env existant dans le projet

# DOC DES LIBS UTILISES

1. https://next-auth.js.org/
2. https://www.material-react-table.com/
3. https://nextjs.org/
