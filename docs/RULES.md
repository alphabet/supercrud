**REGLES DE L'APPLICATION**

_.env_

**MONGODB_URI**=

**KEYCLOAK_SECRET**=

**KEYCLOAK_ID**=

**KEYCLOAK_ISSUER**= le realm keycloak

**NEXTAUTH_URL**= url de l'APP

**NEXTAUTH_SECRET**=

**NEXT_PUBLIC_AUTHORIZED**= tableau d'email autorisé à consulter l'app

**NEXT_PUBLIC_EDITING_ACCESS**= tableau d'email autorisé a éditer les champs

**NEXT_PUBLIC_BASE_URL**= url de l'APP

**Bien penser a mettre a jour le tableau d email pour donner des droits de lecture ou d ecriture .**

_next.config.js_

les **variables d environnement** sont séparés en **2 objets** permettant d'exposer les variables d'environnement **uniquement coté serveur** ou **coté client** .

Ici les env seront exposées que côté serveur

**serverRuntimeConfig**

**MONGODB_URI**

**KEYCLOAK_SECRET**

**KEYCLOAK_ID**

**KEYCLOAK_ISSUER**

**NEXTAUTH_URL**

**NEXTAUTH_SECRET**

Ici les env seront éxposées côté serveur ET côté client

**publicRuntimeConfig**

**NEXT_PUBLIC_AUTHORIZED**

**NEXT_PUBLIC_EDITING_ACCESS**

**NEXT_PUBLIC_BASE_URL**
