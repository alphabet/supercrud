# Changelog

## [1.4.0](https://gitlab.mim-libre.fr/alphabet/supercrud/compare/release/1.3.0...release/1.4.0) (2024-10-14)


### Features

* **crud:** add new collection ([9771616](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/9771616eeffb857460f6c960ca3068eca5c376b7))
* **crud:** type and value are editable ([95d4a1f](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/95d4a1f20e6b689b4320edeb580368a39fa21c31))


### Bug Fixes

* **contextSettings:** manage booleans ([955553f](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/955553f098e3e72ddc9fdad091f48ad6d70e97b2))
* **contextSettings:** manage number ([df13416](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/df13416f43b7b691b9efa822dbc7c5215bc66462))
* **libraries:** update vulnerable libraries ([03ffcbb](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/03ffcbbccbdbfaf310c1089664f378946e9e10e1))

## [1.3.0](https://gitlab.mim-libre.fr/alphabet/supercrud/compare/release/1.2.0...release/1.3.0) (2024-06-17)


### Features

* **nextjs:** upgrade from v12 to v13 ([bc5ce2d](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/bc5ce2dece08525f1ade57040fd89eaa35e1b1cd))
* **nextjs:** upgrade from v13 to 14 ([89cee03](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/89cee03655ddaf5bcbc2943048b24a88c40e04f8))


### Bug Fixes

* **dockerfile:** change CMD from server.js to start script ([3801256](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/38012561ff28e20dfb5c5469e139e36d796cc415))
* **dockerfile:** fix start CMD in Dockerfile ([e91531e](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/e91531e7b6aa703c491f29d41e522c22d07e3d50))
* **dockerfile:** get back to previous docker build method ([3b9b274](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/3b9b274736c78a170d2c410d012c0c2306e339a1))
* **docker:** update dockerfile ([b72dd62](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/b72dd6278182ec040fde01ab2b6f112852f4fc3a))
* **packages:** update package.json for NextJS 14 ([7f228e8](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/7f228e8a4bfacb9bfa2ef5b5c89e60e9b33703a9))
* **scripts:** change start script ([94c26fe](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/94c26fe5f83c6303284960282e83901188eb2437))
* **tables:** add optional chaining into all tables ([e3c6c2b](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/e3c6c2bda0ddae4c5378d02f26d9083b44c7c61f))
* **users:** add optional chaining if emails is empty ([2570d9f](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/2570d9f697e4a485c951ad514ca3c68c812a8660))

## [1.2.0](https://gitlab.mim-libre.fr/alphabet/supercrud/compare/release/1.1.0...release/1.2.0) (2024-01-30)


### Features

* **collection:** add new collections ([eb53600](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/eb536003fb751393aa0517cc004f4c3499eca48e))


### Bug Fixes

* **doc:** fix typo ([bfe4c77](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/bfe4c77e8e9573b418215a96169e459ad1e962ba))


### Documentation

* **app:** add doc for maintain project ([29492b0](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/29492b0cbfcca657a05727e16a9a389f7f4d228b))

## [1.1.0](https://gitlab.mim-libre.fr/alphabet/supercrud/compare/release/1.0.0...release/1.1.0) (2023-08-24)


### Features

* **skills:** add new collection skills ([79dbaf7](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/79dbaf74048981330d81b1f9090d9142486c7b43))


### Bug Fixes

* **audit:** update vulnerable mongoose library ([845c853](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/845c853f9d863096277d8c6d01e5e9d1ed8e2470))

## 1.0.0 (2022-11-22)


### Features

* **nextconfig:** add feeback to server side for show which env is load during build process ([c161871](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/c1618712f2bd210e5063209b677719b2f4a187ee))
* **nextconfig:** load env at runtime for docker process ([05d9797](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/05d9797c4889591d90f83f5cc723d6ca069c2eab))
* **style:** center header components ([ccd396c](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/ccd396c0e795319b722d2d910b164a98a815e7b7))
* **style:** correction some style point ([8e16844](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/8e16844e2311c0343b907f153ac7bfe94615393f))
* **table:** add mezig and roleassignment table ([77f5ac2](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/77f5ac2c3a7a5f5cb8c967038962cf7662b70631))


### Bug Fixes

* **conflict:** rebase on dev ([aeadc49](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/aeadc49275417dc808b7b4ae064d29c11ef6cdba))
* **dockerfile:** dont copy .env for pass test on gitlab ([0db8137](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/0db8137e4aead408fd1ec27213eed5e708fe38a6))


### Code Refactoring

* **app:** slice large text for improve lisibility ([1b23c8a](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/1b23c8a5e5f3f2fa93152fb9e2a7c43b6c002e30))
* **style:** add style on new app ([16eec1e](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/16eec1ec4a6e218e413d2d1d96e43bd5bb14176a))


### Continuous Integration

* **build:** generate and push docker image ([d3556a4](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/d3556a40973386633e8ad145758da628b140bde5))
* **commitlint:** enforce commit message format ([925bfc1](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/925bfc187840b69cbabee137135f10188db57150))
* **release:** avoid regression in `dev` branch ([2a0db3d](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/2a0db3d8565a34d0847c1106afbd07b7f5521ccd))
* **release:** create releases semantic-release ([b2ec783](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/b2ec78388b7197d915f2f4b466ad97c9d3279f41))
* **release:** tag docker image ([0f11d25](https://gitlab.mim-libre.fr/alphabet/supercrud/commit/0f11d25bd40002cfc35ae389208b7ec7ac9d75ce))
