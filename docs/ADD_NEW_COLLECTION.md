**AJOUT D'UNE COLLECTION A L'OUTIL SUPERCRUD**

**CREATION DU SCHEMA (MODEL)**

_./database/models/YourModel_

le champ collection est le nom exact de la collection dans la DB.

**CREATION ROUTE API (CONTROLLER)**

_./pages/api/folderCollectionName/index.js_

Chaque sous dossier du dossier API est une route HTTP pour le requetage .
On peut traiter différemment chaque collection .

**CREATION DE LA PAGE (VIEW)**

_./pages/_

Chaque fichier dans le dossier page est une route ( navigation ) de l'app .
Chaque page fait appel au composant d'affichage Dashboard .
La page sert à parametrer les actions et le rendu de la collection .
On définit le header du dashboard et on lui associe la clé de la data correspondante .
On peut ajouter des actions ( exemple : copytoclipboard )
On peut éditer la taille des cellules etc .

**AJOUT DE LA PAGE A LA NAVIGATION**

_./components/Navigation.jsx_
