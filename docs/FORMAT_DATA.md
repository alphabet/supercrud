**FORMATTAGE DU DASHBOARD**

_./pages/yourPage_

**data simple** par exemple {id:1}

=> on utilise **accessorKey**

**data imbriquée** par exemple :{role:{id:1, name:"test"}}

=> on utilise **accessorFn**

**besoin de traitement pour l'affichage** par exemple ( couper la chaîne de caractères )

=> on utilise Cell . On peut formater la data ou retourner de l'html .

**Attributs utiles**

**enableEditting**: permet de définir les champs possible à editer . ( par defaut true )
**enableClickToCopy** : permet de copier au survol dans le presse papier ( par defaut false )
**enableGlobalFilter**: permet de retirer ce champ de la recherche globale ( par defaut true )
**muiTableBodyCellEditTextFieldProps**: permet de definir le type d'input lors de l'update du champ (par defaut string )
