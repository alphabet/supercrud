# Guide de création d'une nouvelle collection dans l'app

> Création du Model => ./database/models/YourModel

```jsx
import mongoose from "mongoose";

const BookmarksSchema = new mongoose.Schema(
  {
    _id: { type: String },
    url: { type: String },
    name: { type: String },
    author: { type: String },
    groupId: { type: String },
    tag: { type: String },
    icon: { type: String },
  },
  { collection: "bookmarks" }, // exact name in DB
  { timestamps: true }
);

export default mongoose.models.Bookmarks ||
  mongoose.model("Bookmarks", BookmarksSchema);
```

> Création de votre route API => ./pages/api/collectionName/index.js

```jsx
import { getServerSession } from "next-auth/next"; // For check client session for block API access to non authenticated user
import { authOptions } from "../auth/[...nextauth]";

import connectDB from "../../../database/connectDB";
import Bookmarks from "../../../database/models/Bookmarks";

export default async function handler(req, res) {
  const session = await getServerSession(req, res, authOptions);
  const { method } = req;
  const { tableName } = req.query;

  if (!session)
    return res.end(
      "You must be signed in to view the protected content on this page."
    );

  if (!tableName) return res.end("Cannot access API by URL"); // For block fetch API by URL , only navigation from client can fetch API

  await connectDB();

  switch (method) {
    case "GET":
      try {
        const bookmarks = await Bookmarks.find({});
        res.status(200).json({ data: bookmarks });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
    case "DELETE":
      try {
        const bodyParsed = JSON.parse(req.body);
        const id = bodyParsed.id;
        const result = await Bookmarks.findOneAndDelete({ _id: id });
        res.status(200).json({
          success: true,
          result,
          message: "supprimé de la Base de Donnée !",
        });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
    case "PUT":
      try {
        const bodyParsed = JSON.parse(req.body);
        const id = bodyParsed._id;
        const result = await Bookmarks.findByIdAndUpdate(
          { _id: id },
          { ...bodyParsed }
        );
        res.status(200).json({
          success: true,
          result,
          message: "Modifié en Base de Donnée !",
        });
      } catch (error) {
        res.status(400).json({ success: false, error: error.message });
      }
      break;
  }
}
```

> Création de la page de votre collection => ./pages/yourPage

```jsx
import React, { useMemo } from "react";
import DashBoard from "../components/DashBoard/DashBoard";

const Bookmarks = () => {
  const columns = useMemo(
    // Definition de vos colonnes a afficher
    () => [
      {
        accessorKey: "_id", // id de la colonne : REQUIS
        header: "id", // nom de la colonne : REQUIS
        enableClickToCopy: true,
        enableEditing: false, // default : true
        size: 200,
      },
      {
        accessorKey: "name",
        header: "name",
      },
      {
        accessorKey: "author",
        header: "author",
        size: 200,
        enableClickToCopy: true,
      },
      {
        accessorKey: "groupId",
        header: "groupId",
        size: 200,
        enableEditing: false,
        enableClickToCopy: true,
      },
      {
        accessorKey: "tag",
        header: "tag",
      },
      {
        // Accessor pour les propriété imbriquées, bien penser au optional chaining operator
        accessorFn: (row) => row.propriete1?.propriete2?.propriete3,
        id: "propriete1.propriete2.propriete3",
        header: "propriete3",
        // Permet d editer les cells de cette colonne
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 20) {
            // Return un ascenseur html si la string fais plus de 20 caracteres
            return (
              <details>
                <summary>{cell.getValue().substr(0, 12)}</summary>
                {cell.getValue()}
              </details>
            );
          }
        },
      },
      {
        accessorFn: (row) => row.propriete1[0].propriete2,
        id: "propriete1.[0].propriete2",
        header: "propriete1.propriete2",
        // Return le booléan en String pour l'afficher dans la table
        Cell: ({ cell }) => cell.getValue()?.toString(),
      },
      {
        accessorKey: "field3",
        header: "field3",
        size: "240",
        Cell: ({ cell }) => {
          if (cell.getValue()?.length > 1) {
            // Return le premier element du tableau et affiche les autres a la ligne
            return (
              <details>
                <summary>{cell.getValue().slice(0, 1)}</summary>
                {cell.getValue().slice().join("\n")}
              </details>
            );
          }
        },
      },
      {
        accessorKey: "field4",
        header: "field4",
        muiTableBodyCellEditTextFieldProps: () => ({
          // Set le type d'input lors de l edition
          type: "number",
        }),
      },
    ],
    []
  );

  return (
    <>
      <DashBoard columns={columns} apiRouteName={"bookmarks"} />
    </>
  );
};

export default Bookmarks;
```

> Ajouter votre page dans la navigation => ./components/Navigation.jsx

```jsx
<Link href="/yourPage"> YourPage</Link>
```
