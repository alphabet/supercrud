export function formatData(tableData, setDataToSend) {
  return async (cell, value) => {
    let dataCopy = tableData[cell.row.index]; // copy data to modify for prevent danger to alterate initial data
    let columnId = cell.column.id;
    let columnValues;

    // object properties to modify is nested
    if (columnId.includes("[")) {
      columnValues = columnId.split(".");
      switch (typeof dataCopy[columnValues[0]][0][columnValues[2]]) {
        case "boolean":
          if (value.toLowerCase() === "true") {
            dataCopy[columnValues[0]][0][columnValues[2]] = true;
          } else {
            dataCopy[columnValues[0]][0][columnValues[2]] = false;
          }
          break;
        default:
          dataCopy[columnValues[0]][0][columnValues[2]] = value;
          break;
      }
      // object properties to modify is not nested
    } else {
      switch (typeof dataCopy[columnId]) {
        case "number":
          dataCopy[columnId] = Number(value);
          break;
        case "boolean":
          if (value.toLowerCase() === "true") {
            dataCopy[columnId] = true;
          } else {
            dataCopy[columnId] = false;
          }
          break;
        case "object":
          // object properties to modify is array
          if (Array.isArray(dataCopy[columnId])) {
            let temp = value.split(",");
            // user dont set any value or remove existing values
            if (value.length === 0) {
              dataCopy[columnId] = [];
              // user set new values or values already exists into this array
            } else {
              dataCopy[columnId] = temp;
            }
          }
          break;
        default:
          dataCopy[columnId] = value;
      }
    }
    return setDataToSend(dataCopy);
  };
}
